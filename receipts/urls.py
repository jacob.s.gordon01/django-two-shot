from django.urls import path
from receipts.views import RecieptListView


urlpatterns = [
    path("", RecieptListView.as_view(), name="home")
]
